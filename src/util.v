(** * Typical.util : Notations and arithmetic lemmas  *)
From mathcomp Require Export all_ssreflect.
(** * Notations and lemmas taken from the PROSA library *)

(** Notation for concatenation as a bigop *)
Reserved Notation "\cat_ ( x <- s ) F"
  (at level 41, F at level 41, x, s at level 50,
   format "'[' \cat_ ( x <- s ) '/ ' F ']'").

Notation "\cat_ ( x <- s ) F" :=
  (\big[cat/[::]]_(x <- s) F%N) : nat_scope.

Reserved Notation "\cat_ ( x <- s | P ) F"
  (at level 41, F, P at level 41, x, s at level 50,
   format "'[' \cat_ ( x <- s | P ) '/ ' F ']'").

Notation "\cat_ ( x <- s | P ) F" :=
  (\big[cat/[::]]_(x <- s | P ) F%N) : nat_scope.

Reserved Notation "\cat_ ( m <= i < n | P ) F"
  (at level 41, F at level 41, P at level 41, i, m, n at level 50,
   format "'[' \cat_ ( m <= i < n | P ) '/ ' F ']'").

Notation "\cat_ ( m <= i < n | P ) F" :=
  (\big[cat/[::]]_(m <= i < n | P) F%N) : nat_scope.

Reserved Notation "\cat_ ( m <= i < n ) F"
  (at level 41, F at level 41, i, m, n at level 50,
   format "'[' \cat_ ( m <= i < n ) '/ ' F ']'").

Notation "\cat_ ( m <= i < n ) F" :=
  (\big[cat/[::]]_(m <= i < n) F%N) : nat_scope.


Reserved Notation "\cat_ ( i < n ) F"
  (at level 41, F at level 41, i, n at level 50,
   format "'[' \cat_ ( i < n ) '/ ' F ']'").

Notation "\cat_ ( i < n ) F" :=
  (\big[cat/[::]]_(i < n) F%N) : nat_scope.

Reserved Notation "\cat_ ( i < n | P ) F"
  (at level 41, F at level 41, i, n at level 50,
   format "'[' \cat_ ( i < n | P ) '/ ' F ']'").

Notation "\cat_ ( i < n | P ) F" :=
  (\big[cat/[::]]_(i < n | P) F%N) : nat_scope.

(** Lemmas about concatenation *)
Lemma size_bigcat {T} (F : nat -> seq T) t1 t2:
  size (\cat_(t1 <= i < t2) F i) = \sum_(t1 <= i < t2) size (F i).
Proof.
  elim: t2 => [|t2 IHt2].
    by rewrite !big_geq.
    case: (leqP t1 t2) => ineq; last by rewrite !big_geq.
      by rewrite !big_nat_recr //= size_cat IHt2.
Qed.

Lemma mem_bigcat_nat:
  forall (T: eqType) x m n j (f: _ -> list T),
    m <= j < n ->
    x \in (f j) ->
          x \in \cat_(m <= i < n) (f i).
Proof.
  move=> T x m n j f /andP[Lo Hi] In.
  rewrite (@big_cat_nat _ _ _ j) //=; last exact: ltnW.
  rewrite mem_cat; apply/orP; right.
  destruct n; first by rewrite big_nil.
  rewrite big_nat_recl; last by rewrite -ltnS.
    by rewrite mem_cat; apply/orP; left.
Qed.

Lemma mem_bigcat_nat_exists :
  forall (T: eqType) x m n (f: nat -> list T),
    x \in \cat_(m <= i < n) (f i) ->
          exists i, x \in (f i) /\
                          m <= i < n.
Proof.
  intros T x m n f IN.
  induction n; first by rewrite big_geq // in IN.
  destruct (leqP m n); last by rewrite big_geq ?in_nil // ltnW in IN.
  rewrite big_nat_recr // /= mem_cat in IN.
  move: IN => /orP [HEAD | TAIL].
  {
    move/IHn: HEAD => [x0 [Hx0 /andP[Lo Hi]]].
    exists x0; split; first by done.
    by rewrite leqW ?Lo.
  }
  {
    exists n; split; first by done.
    apply/andP; split; [by done | by apply ltnSn].
  }
Qed.

Lemma bigcat_nat_uniq :
  forall (T: eqType) n1 n2 (F: nat -> list T),
    (forall i, uniq (F i)) ->
    (forall x i1 i2,
        x \in (F i1) -> x \in (F i2) -> i1 = i2) ->
    uniq (\cat_(n1 <= i < n2) (F i)).
Proof.
  intros T n1 n2 f SINGLE UNIQ.
  case (leqP n1 n2) => [LE | GT]; last by rewrite big_geq // ltnW.
  rewrite -[n2](addKn n1).
  rewrite -addnBA //; set delta := n2 - n1.
  induction delta; first by rewrite addn0 big_geq.
  rewrite addnS big_nat_recr /=; last by apply leq_addr.
  rewrite cat_uniq; apply/andP; split; first by apply IHdelta.
  apply /andP; split; last by apply SINGLE.
  rewrite -all_predC; apply/allP; intros x INx.
  simpl; apply/negP; unfold not; intro BUG.
  apply mem_bigcat_nat_exists in BUG.
  move: BUG => [i [IN /andP [_ LTi]]].
  apply UNIQ with (i1 := i) in INx; last by done.
    by rewrite INx ltnn in LTi.
Qed.

(** * Additional arithmetic lemmas required by the proofs *)

Lemma leq_sum_seq (I: eqType) (r: seq I) (P : pred I) (E1 E2 : I -> nat) :
  (forall i, i \in r -> P i -> E1 i <= E2 i) ->
  \sum_(i <- r | P i) E1 i <= \sum_(i <- r | P i) E2 i.
Proof.
  intros LE.
  rewrite big_seq_cond [\sum_(_ <- _| P _)_]big_seq_cond.
    by apply leq_sum; move => j /andP [IN H]; apply LE.
Qed.

Lemma leq_big_max I r (P : pred I) (E1 E2 : I -> nat) :
  (forall i, P i -> E1 i <= E2 i) ->
  \max_(i <- r | P i) E1 i <= \max_(i <- r | P i) E2 i.
Proof.
  move => leE12; elim/big_ind2 : _ => // m1 m2 n1 n2.
  intros LE1 LE2; rewrite leq_max; unfold maxn.
    by destruct (m2 < n2) eqn:LT; [by apply/orP; right | by apply/orP; left].
Qed.

Lemma filter_bigcat {T R} (F : R -> seq T) (P : pred T) (s : seq R):
  [seq x <- \cat_(i <- s) F i | P x] = \cat_(i <- s) [seq x <- F i | P x].
Proof.
  elim: s => [|i s IHs]; first by rewrite !big_nil.
    by rewrite !big_cons filter_cat IHs.
Qed.

Lemma split_bigcat_nat {T : Type} {F : nat -> seq T} {P : pred nat} {t1 t3 : nat} t2 :
  t1 <= t2 ->
  t2 <= t3 ->
  \cat_(t1 <= t < t3 | P t) F t = cat (\cat_(t1 <= t < t2 | P t) F t) (\cat_(t2 <= t < t3 | P t) F t).
Proof.
  move=> H /subnK<-.
  move:(t3 - t2) => n.
  elim:n =>[|n IHn]; first by rewrite add0n [X in cat _ X]big_geq ?cats0.
  rewrite addSn big_mkcond big_nat_recr ?(leq_trans H) ?leq_addl//= -big_mkcond/=.
  rewrite IHn.
  rewrite [X in _ = cat _ X]big_mkcond big_nat_recr ?(leq_trans H) ?leq_addl//= -big_mkcond/=.
    by rewrite catA.
Qed.

Lemma count_sum {T : Type} (a : pred T) (s : seq T) :
  count a s = \sum_(x <- s) a x.
Proof.
  rewrite -sum1_count big_mkcond/=.
  exact: eq_big.
Qed.

Lemma sum_perm (T U : eqType) (r : seq T) (s : seq U) (f : T -> U) F :
  uniq s ->
  \sum_(x <- r | f x \in s) F x = \sum_(y <- s) \sum_(x <- r | y == f x) F x.
Proof.
  move=> Hs.
  have->: \sum_(y <- s) \sum_(x <- r | y == f x) F x =
  \sum_(y <- s) \sum_(x <- r) if y == f x then F x else 0
    by apply: eq_big_seq=>/= y _; rewrite big_mkcond.
  rewrite exchange_big/=.
  rewrite big_mkcond/=.
  apply: eq_big_seq=> x Hx.
  case: ifP => Hfx; first by rewrite (bigD1_seq (f x))//= eqxx big1=>//= y /negPf->.
  rewrite big1_seq//==>y Hy.
  case: ifP=>//=/eqP eq.
    by rewrite -eq Hy in Hfx.
Qed.


(** * Suprema *)

Reserved Notation "a <= \sup_ ( x | P ) F"
         (at level 41, F, P at level 41, x at level 50,
          format "'[' a '/ '  <= '/ '  \sup_ ( x | P ) '/ '  F ']'").

Definition leq_sup {T : Type} (P : T -> Prop) F a :=
  forall y, (forall x, P x -> F x <= y) -> a <= y.

Notation "a <= \sup_ ( x | P ) F" :=
  (leq_sup (fun x => P) (fun x => F%N) a).

Goal 1 <= \sup_(n | n <= 1) n.
  by move=> b /(_ 1) /(_ is_true_true).
Qed.


(** * Multisets *)

(** [multiset ts] is the type of multisets over the elements of [ts] *)
Definition multiset {X : choiceType} (xs : seq X) := {ffun seq_sub xs -> nat}.