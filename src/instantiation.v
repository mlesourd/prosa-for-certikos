Require Import model.

Record task := { task_id : nat;
                 task_period : duration;
                 task_cost : duration;
                 task_deadline : duration }.

Definition eqt (t1 t2 : task) :=
  (task_id t1 == task_id t2) &&
  (task_period t1 == task_period t2) &&
  (task_cost t1 == task_cost t2) &&
  (task_deadline t1 == task_deadline t2).

Lemma eqtP : Equality.axiom eqt.
Proof.
  move=> [i1 p1 c1 d1] [i2 p2 c2 d2].
  apply/(iffP idP)=>[|->].
  - by rewrite /eqt/==>/andP[/andP[/andP[/eqP->/eqP->]/eqP->]/eqP->].
  - by rewrite /eqt !eqxx.
Qed.

Canonical task_eqMixin := EqMixin eqtP.
Canonical task_eqType := Eval hnf in EqType _ task_eqMixin.

Record job :=
  { job_id: nat;
    job_arrival: instant;
    job_cost: duration;
    job_task: task }.

Definition eqj (j1 j2 : job) :=
  (job_id j1 == job_id j2) &&
  (job_arrival j1 == job_arrival j2) &&
  (job_cost j1 == job_cost j2) &&
  (job_task j1 == job_task j2).

Lemma eqjP : Equality.axiom eqj.
Proof.
  move=> [i1 a1 c1 t1] [i2 a2 c2 t2].
  apply/(iffP idP)=>[|->].
  - by rewrite /eqj/==>/andP[/andP[/andP[/eqP->/eqP->]/eqP->]/eqP->].
  - by rewrite /eqj !eqxx.
Qed.

Canonical job_eqMixin := EqMixin eqjP.
Canonical job_eqType := Eval hnf in EqType _ job_eqMixin.
