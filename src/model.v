(** * Typical.model : Definitions and operations on traces, taken from the Prosa library *)
Require Export util.

(** We consider discrete time represented as native Coq natural numbers. *)
Definition instant := nat.
Definition duration := nat.

(** The type of tasks, [choiceType] is the SSReflect formalization of Types with countably
    many elements. *)
Definition TaskType := eqType.

(** Tasksets are sequences of tasks *)
Definition taskset (T : TaskType) := seq T.

(** The type of jobs, [eqType] is the SSReflect formalization of Types with decidable equality *)
Definition JobType := eqType.

(** * Definition of arrival traces *)
Definition arrival_sequence (Job : JobType) := instant -> seq Job.

Section Arrival_sequence.

  Context {T : TaskType} {J : JobType}.

  Section Definitions.
    Variable job_arrival : J -> instant.
    Variable job_task : J -> T.

    Variable (arrs : arrival_sequence J).

    (** Definitions for arrival of jobs *)
    Definition in_arr_seq j :=
      exists t, j \in arrs t.

    Definition arrives_at j t := job_arrival j == t.
    Definition arrives_between j t1 t2 := t1 <= job_arrival j < t2.

    Definition arrivals_between t1 t2 := \cat_(t1 <= t < t2) arrs t.

    Definition task_arrivals_at (tsk : T) t := [seq j <- arrs t | job_task j == tsk].
    Definition task_arrivals_between (tsk : T) t1 t2 :=
      \cat_(t1 <= t < t2) task_arrivals_at tsk t.

    (** Definitions for the consistency of arrival sequences *)
    Definition uniq_arrival := forall t, uniq (arrs t).
    Definition consistent_arrival :=
      forall j t,
        j \in arrs t ->
        job_arrival j = t.

    (** A valid arrival sequence is compatible with job_arrival and has at most one instance of every job arriving at every instant *)
    Definition valid_arrival_sequence := uniq_arrival /\ consistent_arrival.
  End Definitions.

  Section Theory.
    Context {job_arrival : J -> instant}.
    Context {job_task : J -> T}.

    Context {arrs : arrival_sequence J}.
    Hypothesis arr_seq_valid : valid_arrival_sequence job_arrival arrs.

    (** Lemmas about valid arrival sequences *)

    Lemma task_arrivals_at_uniq1 t tsk : uniq (task_arrivals_at job_task arrs tsk t).
    Proof.
        by rewrite filter_uniq ?arr_seq_valid.1.
    Qed.

    Lemma task_arrivals_at_uniq2 j tsk t1 t2 :
      j \in task_arrivals_at job_task arrs tsk t1 ->
      j \in task_arrivals_at job_task arrs tsk t2 ->
      t1 = t2.
    Proof.
        by rewrite !mem_filter => /andP[_ /arr_seq_valid.2 <-] /andP[_ /arr_seq_valid.2 <-].
    Qed.

    Lemma uniq_arrival2 : forall j t t',
        j \in arrs t -> j \in arrs t' -> t = t'.
    Proof.
        by move=> j t t' /arr_seq_valid.2 <- /arr_seq_valid.2 <-.
    Qed.

    Lemma arrivalsP j t1 t2 :
      j \in arrivals_between arrs t1 t2 =
            (t1 <= job_arrival j < t2) &&
            (j \in arrs (job_arrival j)).
    Proof.
      rewrite /arrivals_between.
      apply/idP/idP => [/mem_bigcat_nat_exists[t' [Harr']]|/andP[H H']].
      { suff->: job_arrival j = t' by rewrite Harr' andbT.
          by move/arr_seq_valid.2 : (Harr') => ->.
      } {
        apply: (mem_bigcat_nat _ _ _ _ (job_arrival j)); first exact: H.
        exact: H'.
      }
    Qed.

    Lemma task_arrivalsP j tsk t1 t2 :
      j \in task_arrivals_between job_task arrs tsk t1 t2 =
            (job_task j == tsk) &&
            (t1 <= job_arrival j < t2) &&
            (j \in arrs (job_arrival j)).
    Proof.
      rewrite /task_arrivals_between -filter_bigcat.
      by rewrite mem_filter arrivalsP andbA/=.
    Qed.

    Lemma arrivals_between_uniq t1 t2 : uniq (arrivals_between arrs t1 t2).
    Proof.
      apply: bigcat_nat_uniq=> [t | j t t'].
      exact: arr_seq_valid.1.
      exact: uniq_arrival2.
    Qed.

    Lemma task_arrivals_between_uniq tsk t1 t2 : uniq (task_arrivals_between job_task arrs tsk t1 t2).
    Proof.
      apply: bigcat_nat_uniq=> [t | j t t'].
      exact: task_arrivals_at_uniq1.
      exact: task_arrivals_at_uniq2.
    Qed.


  End Theory.
End Arrival_sequence.

(** * Definition of schedules *)
Definition schedule (J : JobType) := instant -> option J.

Section Schedule.

  Context {T : TaskType} {J : eqType}.

  Variable task_deadline : T -> duration.

  Variable job_arrival : J -> instant.
  Variable job_cost : J -> duration.
  Variable job_task : J -> T.

  Section Definitions.

    Variable sched : schedule J.

    (** Definitions for scheduled jobs *)
    Definition scheduled_at j t := sched t == Some j.

    Definition service_between j t1 t2 := \sum_(t1 <= t < t2) scheduled_at j t.
    Definition service j t := service_between j 0 t.

    Definition completed j t := service j t >= job_cost j.

    Definition response_time_bound b j :=
      completed j (job_arrival j + b).

    Definition deadline_miss j :=
      ~~ response_time_bound (task_deadline (job_task j)) j.

  End Definitions.
End Schedule.