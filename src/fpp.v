(** * Typical.fpp : Definition and analysis of the Fixed Priority Preemptive with arrival curves model *)
Require Export model.

(** * Definitions related to worst case execution time from Prosa *)
Section WCET.
  Context {T : TaskType}.
  Variable task_wcet : T -> duration.

  Context {J: JobType}.
  Variable job_cost: J -> duration.
  Variable job_task : J -> T.

  Variable arrs : arrival_sequence J.

  (** WCET semantics *)
  Definition wcet_correct := forall j,
      in_arr_seq arrs j ->
      job_cost j <= task_wcet (job_task j).
End WCET.

(** * Definitions related to arrival curves *)
Section ArrivalCurves.
  Context {T : TaskType}.
  (** We only define the curves required by our development *)
  Variable eta_plus : T -> duration -> nat.
  Variable delta_plus : T -> nat -> duration.

  Context {J: eqType}.
  (** Jobs have an arrival time, a cost and can be traced back to a task *)
  Variable job_arrival : J -> instant.
  Variable job_task: J -> T.

  (** We consider an arrival trace, [arrs t] is the list of jobs arriving at time t *)
  Variable arrs : arrival_sequence J.

  Notation task_arrivals_between := (task_arrivals_between job_task arrs).

  (** Arrival model semantics *)
  Definition delta_plus_correct tsk :=
    forall t dt,
      let arrs := task_arrivals_between tsk t (dt + t) in
      dt <= delta_plus tsk (size arrs).

  Definition eta_plus_correct tsk :=
    forall t dt,
      let arrs := task_arrivals_between tsk t (dt + t) in
      size arrs <= eta_plus tsk dt.
End ArrivalCurves.


(** * Definition of the FPP scheduling policy from Prosa *)
Section FPP.
  Context {T : TaskType}.
  (** hp is the priority relation on tasks *)
  (** hp tsk tsk' := "tsk' has higher priority than tsk" *)
  Variable hp : rel T.

  Context {J: JobType}.
  Variable job_task : J -> T.
  Variable job_arrival : J -> instant.
  Variable job_cost : J -> duration.

  Variable arrs : arrival_sequence J.

  Variable sched : schedule J.

  Notation completed := (completed job_cost sched).
  Notation scheduled_at := (scheduled_at sched).

  Definition pending j (t : instant) := (job_arrival j <= t) && ~~ completed j t.
  Definition backlogged j (t : instant) := pending j t && ~~ scheduled_at j t.

  (** Part 1 : the scheduler is work conserving *)
  Definition work_conserving :=
    forall j t,
      in_arr_seq arrs j ->
      backlogged j t ->
      exists j', scheduled_at j' t.

  (** Part 2 : it is compatible with hp *)
  Definition sched_fp :=
    forall j j' t,
      in_arr_seq arrs j ->
      backlogged j t ->
      scheduled_at j' t ->
      hp (job_task j) (job_task j').

End FPP.

(** * Busy window analysis for FPP + arrival curves *)
(** This part is closely related to the response time analysis for sporadic tasks under the FPP policy found in Prosa. We adapted it to use arrival curves and provide a local response time bound which we use for the local deadline miss analysis *)
Section Analysis.
  Context {T : TaskType}.
  (** We consider tasks with a WCET and arrival curves, *)
  Variable task_wcet : T -> duration.
  Variable eta_plus : T -> duration -> nat.

  (** a taskset [ts] *)
  Variable ts : seq T.
  Hypothesis ts_uniq : uniq ts.

  (** and a total order [hp] on tasks where [hp tsk tsk'] denotes the fact that [tsk'] has higher or equal priority than [tsk]. *)
  Variable hp : rel T.
  Hypothesis hp_refl : reflexive hp.
  Hypothesis hp_trans : transitive hp.
  Hypothesis hp_total_over_ts :
    forall tsk tsk',
      tsk \in ts ->
      tsk' \in ts ->
      hp tsk tsk' || hp tsk' tsk.

  Context {J: eqType}.
  Variable job_task : J -> T.
  Variable job_arrival : J -> instant.
  Variable job_cost : J -> duration.

  (** We consider a trace [arrs, sched].*)
  Variable arrs : arrival_sequence J.
  Variable sched : schedule J.

  Hypothesis arrs_valid : valid_arrival_sequence job_arrival arrs.
  (** We only consider nonempty jobs, *)
  Hypothesis cost_pos : forall j, in_arr_seq arrs j -> job_cost j > 0.
  (** and tasks in [ts]. *)
  Hypothesis arrivals_in_ts : forall j t, j \in arrs t -> job_task j \in ts.

  (** We assume that our trace is compatible with our model arrival model, *)
  Hypothesis wcet_correct : wcet_correct task_wcet job_cost job_task arrs.
  Hypothesis arrival_model :
    forall tsk,
      tsk \in ts ->
      eta_plus_correct eta_plus job_task arrs tsk.

  Notation scheduled_at := (scheduled_at sched).
  Notation service := (service sched).

  (** and [sched] respects the FPP policy, *)
  Hypothesis work_conserving : work_conserving job_arrival job_cost arrs sched.
  Hypothesis sched_fp : sched_fp hp job_task job_arrival job_cost arrs sched.

  (** and basic structural properties about scheduled jobs: *)

  (** - Scheduled jobs are part of the arrival sequence *)
  Hypothesis sched_in_arrs : forall j t, scheduled_at j t -> in_arr_seq arrs j.
  (** - They have arrived *)
  Hypothesis sched_arrived : forall j t, scheduled_at j t -> job_arrival j <= t.
  (** - And are not completed *)
  Hypothesis not_sched_completed : forall j t, service j t <= job_cost j.

  (** ** Definition and analysis of busy windows *)

  Section BusyWindows.

    Notation completed := (completed job_cost sched).
    Section Definitions.
      Variable tsk : T.

      (** The definition of level-tsk busy window (called [busy_interval] here) is from Prosa *)
      Definition quiet_time t :=
        forall j,
          in_arr_seq arrs j ->
          hp tsk (job_task j) ->
          job_arrival j < t ->
          completed j t.

      Definition busy_interval_prefix t1 t2 :=
        t1 < t2 /\
        quiet_time t1 /\
        (forall t, t1 < t < t2 -> ~ quiet_time t).

      Definition busy_interval t1 t2 :=
        busy_interval_prefix t1 t2 /\
        quiet_time t2.

    End Definitions.
    (** ** Properties of FPP schedules related to busy windows  *)
    Section Theory.

      Notation pending := (pending job_arrival job_cost sched).
      Variable t1 t2 : instant.

      Variable j : J.
      Variable t : instant.
      Hypothesis Hin_arr : in_arr_seq arrs j.

      Hypothesis Hpend : pending j t.
      Hypothesis Hbi : busy_interval (job_task j) t1 t2.
      Hypothesis Harr : t1 <= t < t2.

      Lemma arrives_in_interval : t1 <= job_arrival j.
      Proof.
        apply: contraT; rewrite -ltnNge => Hi.
        move: Hbi => [[_ [Hquiet _]] _].
        move: Hpend => /andP [_ /negbTE <-].
        move: Harr => /andP [Lo _].
        have: completed j t1 by exact: Hquiet.
        rewrite/completed/service/service_between (big_cat_nat _ _ _ _ Lo)//= => H.
          by rewrite (leq_trans H) ?leq_addr.
      Qed.

      Lemma completes_in_interval : completed j t2.
      Proof.
        move: Hbi => [_ Hquiet].
        move: Harr => /andP [_ ineq].
        apply: Hquiet => //=.
        suff: job_arrival j <= t
          by move=> /leq_ltn_trans /(_ ineq).
        move: Hpend.
        by rewrite /pending => /andP [].
      Qed.
    End Theory.

    Notation backlogged := (backlogged job_arrival job_cost sched).

    Variable tsk : T.

    Lemma quiet_idle t : sched t == None -> quiet_time tsk t.
    Proof.
      move=> /eqP Hidle j Harr Hprio Harrived.
      case: (boolP (completed j t)) => //= Hnotcomp.
      have/(work_conserving j t Harr) [j']: backlogged j t
        by rewrite /backlogged /pending /scheduled_at ltnW //= ?Harrived Hnotcomp Hidle.
        by rewrite /scheduled_at Hidle.
    Qed.

    Lemma prefix_not_idle t1 t2:
      t1 < t2 ->
      quiet_time tsk t1 ->
      (forall t, t1 < t <= t2 -> ~ quiet_time tsk t) ->
      forall t,
        t1 <= t <= t2 ->
        ~~ (sched t == None).
    Proof.
      move=> Ht1t2 Hquiet Hbusy t /andP [].
      rewrite leq_eqVlt => /orP [/eqP<-_{t}|Lo Hi];
        last by (apply/negP=> /(quiet_idle); apply: Hbusy; rewrite Lo Hi).
      apply/negP => /eqP Hidle.
      have/Hbusy Hnext: t1 < t1.+1 < t2.+1
        by rewrite leqnn ltnS Ht1t2.
      apply: Hnext => j Harr Hprio Harrival.
      suff: completed j t1 by
          (rewrite/completed/service/service_between big_nat_recr//= => ineq;
           rewrite (leq_trans ineq)//= leq_addr).
      apply: contraT => Hcomp.
      case: (boolP (scheduled_at j t1)) => [|Hsched];
        first by rewrite /scheduled_at Hidle.
      rewrite ltnS in Harrival.
      case: (work_conserving j t1) => [||j']; first done.
        by rewrite/backlogged/pending Harrival Hcomp Hsched.
        by rewrite /scheduled_at Hidle.
    Qed.

    Lemma completion_monotonic j t t' :
      t <= t' ->
      completed j t ->
      completed j t'.
    Proof.
      rewrite /completed /service /service_between => Htt' Hservice.
        by rewrite -[job_cost j]addn0 (big_cat_nat _ _ _ _ Htt') //= leq_add.
    Qed.

    Lemma sched_uniq j j' t :
      scheduled_at j t ->
      scheduled_at j' t ->
      j = j'.
    Proof.
        by rewrite /scheduled_at => /eqP -> /eqP [].
    Qed.

    Lemma sched_not_completed j t :
      scheduled_at j t ->
      ~~ completed j t.
    Proof.
      rewrite /completed => Hsched.
      apply/negP => Hservice.
      have /eqP eq: service j t == job_cost j
        by rewrite eqn_leq Hservice (not_sched_completed j t).
      move: (not_sched_completed j t.+1).
      rewrite -eq /service /service_between big_nat_recr //=.
        by rewrite Hsched addn1 ltnn.
    Qed.

    Notation pending := (pending job_arrival job_cost sched).

    Lemma sched_pending j t :
      scheduled_at j t ->
      pending j t.
    Proof.
      move=> Hsched.
      by rewrite /pending sched_arrived //= sched_not_completed.
    Qed.

    Lemma busyP t1 t2 :
      t1 < t2 ->
      quiet_time tsk t1 ->
      (forall t, t1 < t <= t2 -> ~ quiet_time tsk t) ->
      forall t,
        t1 <= t < t2 ->
        exists j,
          t1 <= job_arrival j < t2 /\
          hp tsk (job_task j) /\
          scheduled_at j t.
    Proof.
      move=> Ht1t2 Hquiet Hbusy t /andP [Lo Hi].
      have Hnot_idle := prefix_not_idle _ _ Ht1t2 Hquiet Hbusy.
      have Hnot_idle_t : ~~ (sched t == None) by
          apply: Hnot_idle; rewrite Lo ltnW ?Hi.
      case Hsched: (sched t) => [j|];
        last by move: Hsched => /eqP /(negP Hnot_idle_t).
      exists j.
      move/eqP in Hsched.
      have Hprio : hp tsk (job_task j).
      { apply: contraT => /negP Hprio; exfalso.
        apply: (Hbusy t.+1) => [|j' Harr' Hprio' Hbefore];
          first by rewrite ltnS Lo Hi.
        apply: contraT => /negP Hcompl; exfalso.
        have Hback : backlogged j' t.
        { rewrite ltnS in Hbefore.
          rewrite /backlogged /pending Hbefore /=.
          apply/andP; split; apply/negP => Habs.
          - apply: Hcompl.
            exact: (completion_monotonic _ t).
          - suff eq : j = j'
              by rewrite eq Hprio' in Hprio.
            exact: (sched_uniq _ _ t).
        }
        move: (sched_fp j' j t Harr' Hback Hsched) => Hprio''.
        apply: Hprio.
        exact: (hp_trans _ _ _ Hprio').
      }
      repeat split; [|done|done].
      move: (Hsched) => /sched_pending Hpend.
      apply/andP; split; last by move: Hpend => /andP [/leq_ltn_trans /(_ Hi)].
      rewrite leqNgt; apply/negP => Habs.
      have Hcomp: completed j t1 by
          apply: Hquiet; [exact: (sched_in_arrs _ t) | | ].
      suff: ~~ scheduled_at j t by move /negP /(_ Hsched).
      apply/negP => /sched_not_completed.
        by move/(completion_monotonic _ _ _ Lo): Hcomp => ->.
    Qed.

    Lemma arrival_consistent j :
      in_arr_seq arrs j ->
      j \in arrs (job_arrival j).
    Proof.
      move=> [t Ht].
        by rewrite (arrs_valid.2 j _ Ht).
    Qed.

    Lemma busy_pending t1 t2 :
      t1 <= t2 ->
      quiet_time tsk t1 ->
      ~ quiet_time tsk t2 ->
      exists j : J,
        in_arr_seq arrs j /\
        arrives_between job_arrival j t1 t2 /\
        hp tsk (job_task j) /\ ~ completed j t2.
    Proof.
      move=> Ht1t2 Hquiet Hbusy.
      case/boolP: (has (fun j => (~~ completed j t2) && hp tsk (job_task j))
                  (arrivals_between arrs t1 t2)).
      { move=> /hasP [j].
        rewrite (arrivalsP arrs_valid) //= => /andP [Harr Hinarr] /andP [Hnotcomp Hprio].
          by exists j; repeat split; [exists (job_arrival j) | | | apply/negP].
    } {
        rewrite -all_predC => /allP H.
        exfalso.
        apply: Hbusy => j Hinarr Hprio Harr.
      case: (ltnP (job_arrival j) t1) => [Hi | Lo].
        - apply: (completion_monotonic j _ _ Ht1t2).
          exact: Hquiet.
        - have := H j.
          rewrite (arrivalsP arrs_valid) ?Lo ?Harr //=.
          rewrite arrival_consistent //= => /(_ is_true_true).
            by rewrite negb_and negbK orbC Hprio /=.
      }
    Qed.

    Lemma busy_uniq t {t1 t2 t1' t2'} :
      t1 <= t < t2 ->
      t1' <= t < t2' ->
      busy_interval tsk t1 t2 ->
      busy_interval tsk t1' t2' ->
      (t1 = t1') /\ (t2 = t2').
      move => /andP [Lo Hi] /andP [Lo' Hi']
               [[_ [Hquiet1 Hbusy]] Hquiet2] [[_ [Hquiet1' Hbusy']] Hquiet2'].
      split; apply/eqP;
      [set x := t1; have H : x = t1 by []| set x := t2; have H : x = t2 by []];
      rewrite eqn_leq [_ <= x]leqNgt [x <= _]leqNgt -negb_or;
      apply/negP => /orP [|]; rewrite H {H x} => ineq.
      - by apply: (Hbusy' t1); first rewrite ineq (leq_ltn_trans Lo Hi').
      - by apply: (Hbusy t1'); first rewrite ineq (leq_ltn_trans Lo' Hi).
      - by apply: (Hbusy t2'); first rewrite ineq (leq_ltn_trans Lo Hi').
      - by apply: (Hbusy' t2); first rewrite ineq (leq_ltn_trans Lo' Hi).
    Qed.

    Notation arrived_jobs t := (arrivals_between arrs 0 t).

    Definition is_done t j := hp tsk (job_task j) ==> (job_arrival j < t) ==> completed j t.

    Definition quietb t := all (is_done t) (arrived_jobs t).

    Lemma quietP t : reflect (quiet_time tsk t) (quietb t).
    Proof.
      rewrite /quiet_time /is_done.
      apply: (iffP allP).
      {
        move=> H j [] t0 Harrs Hprio Harrived.
        move: (Harrived).
        apply/implyP.
        move: Hprio.
        apply/implyP.
        apply: H.
        move/mem_bigcat_nat: (Harrs) => /(_ 0 t)/=.
        move: arrs_valid.2 => /(_ j t0 Harrs) <-.
          by rewrite Harrived => /(_ is_true_true).
      } {
        move=> H j /mem_bigcat_nat_exists [t0 [Harrs _]].
        apply/implyP => Hprio.
        apply/implyP => Harrived.
        apply: H => //.
          by exists t0.
      }
    Qed.


    (** ** Effective computation of busy windows *)
    Section ConcreteBusy.


      (** ** Effective computation of busy windows *)

      Lemma quiet0 : quietb 0.
      Proof. by apply/quietP. Qed.

      Local Hint Resolve quiet0.

      Definition busy_left t : nat :=
        [arg max_(t' > Ordinal (ltn0Sn t) | quietb t') t'].

      Lemma leq_busy_left t :
        busy_left t <= t.
      Proof.
        rewrite /busy_left.
        case: arg_maxP => // tl _ _.
          by rewrite -ltnS (ltn_ord tl).
      Qed.

      Lemma busy_left_busy t t' :
        busy_left t < t' <= t ->
        ~ quiet_time tsk t'.
      Proof.
        rewrite /busy_left.
        case: arg_maxP => // tl /quietP _ Hmax.
        move=> /andP [Lo Hi] /quietP Hquiet.
        suff: (geq tl t') by rewrite /geq /= leqNgt Lo.
        rewrite -ltnS in Hi.
        exact: (Hmax (Ordinal Hi)).
      Qed.

      Lemma busy_left_quiet t :
        quiet_time tsk (busy_left t).
        rewrite /busy_left.
          by case: arg_maxP => // tl /quietP.
      Qed.

      Variable B : nat.
      Variable t : nat.
      Hypothesis bound : forall t', busy_interval_prefix tsk (busy_left t) t' ->
                                    t' - (busy_left t) <= B.

      Definition busy_right : nat :=
        t.+1 + [arg max_(t' > Ordinal (ltn0Sn B) | [forall t'' :  'I_t', ~~ quietb (t.+1 + t'')]) t'].

      Lemma ltn_busy_right :
        t < busy_right.
      Proof. by rewrite leq_addr. Qed.

      Lemma ltn_left_right :
        busy_left t < busy_right.
      Proof.
        exact: (leq_ltn_trans (leq_busy_left t) ltn_busy_right).
      Qed.

      Lemma busy_right_busy t' :
        t < t' < busy_right ->
        ~ quiet_time tsk t'.
      Proof.
        rewrite /busy_right.
        case: arg_maxP => [|dtr /forallP Htr Hmax /andP [Lo Hi] /quietP Hquiet];
          first by apply/forallP => [[]].
        move/subnKC: Lo => eq.
        have ltn_dt : t' - t.+1 < dtr.
          by apply: contraT; rewrite -(ltn_add2l t) -ltnS -addnS -addSnnS eq -addnS -addSnnS Hi.
          move: (Htr (Ordinal ltn_dt)).
            by rewrite /nat_of_ord eq Hquiet.
      Qed.

      Lemma busy_prefix_right :
        busy_interval_prefix tsk (busy_left t) busy_right.
      Proof.
        split; [|split].
        - exact: ltn_left_right.
        - exact: busy_left_quiet.
        - move=> i /andP [Lo Hi].
          case: (leqP i t) => [{Hi} Hi'|{Lo} Lo'].
            by apply: (busy_left_busy t); rewrite Lo Hi'.
            by apply: busy_right_busy; rewrite Lo' Hi.
      Qed.

      Lemma extend_prefix t':
        busy_interval_prefix tsk t t' ->
        ~ quiet_time tsk t' ->
        busy_interval_prefix tsk t t'.+1.
      Proof.
        move=> [Hlt [Hquiet Hbusy1]] Hbusy2.
        split; [|split; [done|]].
        - exact: leq_trans Hlt.
        - move=> t0 /andP [Lo].
          rewrite ltnS leq_eqVlt => /orP [/eqP->//|Hi'].
            by apply: Hbusy1; rewrite Lo Hi'.
      Qed.

      Lemma busy_right_quiet :
        quiet_time tsk busy_right.
      Proof.
        apply/quietP.
        apply: contraT => Hquiet.
        suff: t + B < busy_right by
          rewrite ltnNge -{1}(subnK (leq_busy_left t)) addnAC addnC -leq_subLR;
          rewrite -[_ - _]add0n leq_add// bound //;
                          apply : busy_prefix_right.
          move: Hquiet.
          rewrite /busy_right.
          case: arg_maxP => [|dtr /forallP Htr Hmax Hquiet];
        first by apply/forallP => [[]].
          rewrite addSnnS ltn_add2l leqNgt.
          apply/negP => HltnB.
          suff: dtr < dtr; first by rewrite ltnn.
          apply: (Hmax (Ordinal HltnB)).
          apply/forallP=>/=[[t']]/=.
          rewrite ltnS leq_eqVlt => /orP [/eqP->//| Ht'].
          exact: Htr (Ordinal Ht').
      Qed.

      Lemma bw_dec :
        busy_interval tsk (busy_left t) busy_right.
      Proof.
        split.
        exact: busy_prefix_right.
        exact: busy_right_quiet.
      Qed.

    End ConcreteBusy.

    (** ** Response time analysis based on busy windows *)
    Section RTA.

      Definition hp_workload tsk t dt :=
        \sum_(j <- arrivals_between arrs t (dt + t) | hp tsk (job_task j)) job_cost j.

      Definition hp_workload_bound tsk dt :=
        \sum_(tsk' <- ts | hp tsk tsk') (eta_plus tsk' dt) * task_wcet tsk'.

      Section WorkloadBound.

        Hypothesis Htsk : tsk \in ts.

        (** Instead of computing fixpoints on workload we assume to be given one. *)
        (** This avoids dealing with the fact that all Coq functions have to be terminating. *)
        Variable R : duration.
        Hypothesis fixR : hp_workload_bound tsk R <= R.

        Lemma fp_workload_bound t :
          hp_workload tsk t R <= R.
        Proof.
          rewrite /hp_workload.
          apply: (leq_trans _ fixR).
          rewrite /hp_workload_bound.
          set arrs' := arrivals_between arrs t (R + t).
          set X := \sum_(tsk' <- ts | hp tsk tsk')
                    \sum_(j <- arrs' | job_task j == tsk') job_cost j.
          apply: (@leq_trans X); rewrite /X {X}.
          { rewrite (exchange_big_dep (fun j => hp tsk (job_task j))) /=;
                    last by move=> tsk' j H /eqP ->.
            rewrite big_seq_cond [X in _ <= X]big_seq_cond.
            apply: leq_sum => j /andP [Hj Hprio].
            have Htskj: job_task j \in ts.
            { apply: arrivals_in_ts.
              move: Hj.
              rewrite /arrs' (arrivalsP arrs_valid)// => /andP[_].
              exact. }
            rewrite big_mkcond (big_rem (job_task j)) ?mem_index_enum//=.
            rewrite Hprio eqxx /=.
            exact: leq_addr. }
          apply: leq_sum_seq => tsk' Htsk' Hprio.
          set X := \sum_(j <- arrs' | job_task j == tsk') task_wcet tsk'.
          apply: (@leq_trans X); rewrite /X {X}.
          { rewrite big_seq_cond [X in _ <= X]big_seq_cond.
            apply: leq_sum => j /andP.
            rewrite (arrivalsP arrs_valid)// =>[[/andP[_ ?]]/eqP eq].
            rewrite -eq wcet_correct//.
            by exists (job_arrival j). }
          rewrite -{1}[task_wcet _]mul1n -big_distrl/=.
          rewrite leq_mul2r.
          case: eqP => //= _.
          rewrite -big_filter sum1_size.
          have->: size [seq i <- arrs' | job_task i == tsk'] =
          size (task_arrivals_between job_task arrs tsk' t (R + t)).
          { rewrite /task_arrivals_between size_bigcat.
            rewrite /arrs' /arrivals_between.
            elim: R => [|n IH]; first by rewrite !big_geq//=. (** should be a lemma *)
            rewrite !big_nat_recr ?leq_addl //=.
              by rewrite filter_cat size_cat IH/=. }
          exact: arrival_model.
        Qed.
      End WorkloadBound.

      Lemma service_between_bound j t1 t2:
        service_between sched j t1 t2 <= job_cost j.
      Proof.
        case: (leqP t2 t1) => [ineq|/ltnW ineq]; first by
          rewrite /service_between big_geq //=.
        have H: service_between sched j 0 t2 <= job_cost j by
          exact: not_sched_completed.
        apply: (leq_trans _ H).
        rewrite /service_between [X in _ <= X](big_cat_nat _ _ (n := t1))//=.
        exact: leq_addl.
      Qed.

      Let hp_service tsk t dt :=
        \sum_(j <- arrivals_between arrs t (dt + t) | hp tsk (job_task j)) service_between sched j t (dt + t).

      Definition degenerate_interval tsk t1 t2 := ~~ has (fun j => hp tsk (job_task j)) (arrivals_between arrs t1 t2).

      Lemma degenerate_short t1 t2 :
        busy_interval_prefix tsk t1 t2 ->
        degenerate_interval tsk t1 t2 ->
        t2 = t1.+1.
      Proof.
        move=> Hbw /hasP Hdegenerate.
        move: Hbw.1.
        rewrite leq_eqVlt=>/orP[/eqP->//| HSt1].
        have Hbusy: ~ quiet_time tsk t1.+1 by
            apply: Hbw.2.2; rewrite leqnn.
        suff: quiet_time tsk t1.+1 by [].
        move=> j Hj Hprio.
        rewrite leq_eqVlt ltnS eqE=>/orP[/=/eqP eq| ineq].
        { suff: exists2 j : J, j \in arrivals_between arrs t1 t2 & hp tsk (job_task j) by [].
          exists j; last by [].
          rewrite (arrivalsP arrs_valid) ?arrival_consistent// andbT.
          by rewrite eq leqnn Hbw.1. }
        { apply: completion_monotonic; first exact: leqnSn.
          exact: Hbw.2.1. }
      Qed.

      Lemma non_degenerate_start  t1 t2 :
        busy_interval_prefix tsk t1 t2 ->
        ~~ degenerate_interval tsk t1 t2 ->
        exists2 j : J, j \in arrs t1 & hp tsk (job_task j).
      Proof.
        move=> Hbw.
        rewrite negbK => /hasP.
        move: Hbw.1.
        rewrite leq_eqVlt=>/orP[/eqP eq| HSt1 _];
          first by rewrite /arrivals_between -eq big_nat1.
        apply/hasP.
        case: (boolP (has _ _))=>[//|/hasP Habs].
        suff/Hbw.2.2: quiet_time tsk t1.+1 by
            rewrite ltnS leqnn HSt1 => /(_ is_true_true).
        move=> j Hj Hprio.
        rewrite leq_eqVlt ltnS eqE=>/orP[/=/eqP eq| ineq].
        { suff: exists2 j : J, j \in arrs t1 & hp tsk (job_task j) by [].
          exists j; last by [].
          by rewrite -eq arrival_consistent. }
        { apply: completion_monotonic; first exact: leqnSn.
          exact: Hbw.2.1. }
      Qed.

      Section Busy_fix.
        Variable t1 t2 : instant.

        Notation hp_workload_here := (hp_workload tsk t1 (t2 - t1)).
        Notation hp_service_here := (hp_service tsk t1 (t2 - t1)).

        Lemma nonstop_hp_service:
          busy_interval_prefix tsk t1 t2 ->
          ~~ degenerate_interval tsk t1 t2 ->
          hp_service_here = t2 - t1.
        Proof.
          move=> Hprefix Hnondegenerate.
          rewrite /hp_service/service_between.
          rewrite exchange_big/=.
          rewrite (subnK (ltnW Hprefix.1)).
          rewrite -[t2-t1]muln1 -sum_nat_const_nat.
          rewrite [in RHS]big_nat big_nat.
          apply: eq_bigr=> t /andP[Lo Hi].
          suff[j [Hj [Hprio Hsched]]]: exists j,
                j \in arrivals_between arrs t1 t2 /\ hp tsk (job_task j) /\
                      scheduled_at j t.
          { rewrite (big_rem j) ?mem_filter ?Hprio ?Hsched//=.
            rewrite big_seq_cond big1//= => j'.
            rewrite rem_filter ?filter_uniq ?(arrivals_between_uniq arrs_valid)//=.
            rewrite !mem_filter/= =>/andP[/andP[/eqP Habs _] _].
            case: (boolP (scheduled_at j' t))=>//= Hsched'.
            suff: j' = j by [].
            exact: (sched_uniq _ _ t). }
          suff[j [Hj [Hprio Hpend]]]: exists j',
              j' \in arrivals_between arrs t1 t2 /\ hp tsk (job_task j') /\ pending j' t.
          { case: (boolP (scheduled_at j t))=>//= Hsched;
              first by exists j.
            have H1: backlogged j t by rewrite /backlogged Hpend Hsched.
            move: (Hj).
            rewrite (arrivalsP arrs_valid)// =>/andP[_ Harrj].
            have H2: in_arr_seq arrs j by exists (job_arrival j).
            case: (work_conserving j t H2 H1) => j' Hj'.
            exists j'.
            have Hprio' : hp tsk (job_task j').
            { apply: hp_trans;
                first exact: Hprio.
              exact: (sched_fp _ _ t). }
            repeat split; [ |done|done].
            - have Harrj' := sched_in_arrs _ _ Hj'.
              rewrite (arrivalsP arrs_valid)// arrival_consistent// andbT.
              apply/andP; split; last by rewrite (leq_ltn_trans _ Hi)// sched_arrived.
              rewrite leqNgt.
              apply/negP => Habs.
              have/negP: ~~ completed j' t by exact: sched_not_completed.
              suff: completed j' t by [].
              apply: completion_monotonic; first exact: Lo.
              exact: Hprefix.2.1. }
          move: Lo.
          rewrite leq_eqVlt=>/orP[/eqP eq|ineq].
          { rewrite -eq.
            case: (non_degenerate_start _ _ Hprefix Hnondegenerate)=> j Hj Hprio.
            have Hpend: pending j t1.
            { rewrite /pending (arrs_valid.2 _ _ Hj) leqnn/=.
              rewrite /completed/service -ltnNge.
              apply: leq_trans; last by (apply: cost_pos; exists t1).
              rewrite ltnS leqn0 /service_between big_nat big1//= => t' Hi'.
              case: (boolP (scheduled_at j t'))=> //= /sched_arrived.
                by rewrite (arrs_valid.2 _ _ Hj) leqNgt Hi'. }
            exists j; repeat split; [|done|done].
            by rewrite /arrivals_between (mem_bigcat_nat _ _ _ _ t1) ?leqnn//eq Hi. }
          { have Hbusy: ~ quiet_time tsk t
              by apply: Hprefix.2.2; rewrite Hi ineq.
            case: (busy_pending t1 t (ltnW ineq) Hprefix.2.1 Hbusy) => j [Hj [/andP[Loj Hij] [Hprio /negP Hnotcompj]]].
            have Harrj: j \in arrivals_between arrs t1 t2
              by rewrite (arrivalsP arrs_valid)// Loj arrival_consistent ?(ltn_trans Hij Hi).
            have Hpend: pending j t
              by rewrite /pending ltnW.
            by exists j. }
        Qed.

        Lemma service_workload_bound:
          busy_interval tsk t1 t2 ->
          hp_service_here = hp_workload_here.
        Proof.
          move=> Hbw.
          rewrite /hp_service.
          rewrite /hp_workload/hp_workload_bound.
          rewrite (subnK (ltnW Hbw.1.1)).
          rewrite [in RHS]big_seq_cond big_seq_cond.
          apply: eq_bigr=> j /andP[Harrj Hprio].
          apply/eqP.
          rewrite eqn_leq service_between_bound/=.
          move: Harrj.
          rewrite (arrivalsP arrs_valid)// =>/andP[/andP[Lo Hi] Harrj].
          suff->: service_between sched j t1 t2 = service j t2
            by apply: Hbw.2; first exists (job_arrival j).
          rewrite /service/service_between.
          rewrite [in RHS](@big_cat_nat _ _ _ t1)//=; last exact: (ltnW Hbw.1.1).
          rewrite [in RHS]big_nat [in RHS]big1//= => t Ht.
          case: (boolP (scheduled_at j t))=>//= /sched_arrived Habs.
          suff: t1 < t1 by rewrite ltnn.
          apply: (leq_ltn_trans Lo).
          apply: (leq_ltn_trans Habs).
          exact: Ht.
        Qed.

        Lemma busy_fix :
          busy_interval tsk t1 t2 ->
          ~~ degenerate_interval tsk t1 t2 ->
          hp_workload_here = t2 - t1.
        Proof.
          move=> Hbw Hnon_degenerate.
          rewrite -service_workload_bound//.
          rewrite nonstop_hp_service//=.
          exact: Hbw.1.
        Qed.
      End Busy_fix.

      Section BusyBound.

        Variable R : duration.
        Hypothesis HR : R > 0.

        Variable t1 t2 : instant.
        Hypothesis Hworkload :
          hp_workload tsk t1 R <= R.

        Hypothesis Hbw : busy_interval_prefix tsk t1 t2.

        Let hp_service_here := hp_service tsk t1 R.
        Let hp_workload_here := hp_workload tsk t1 R.

        Lemma busy_bound : t2 - t1 <= R.
        Proof.
          rewrite leq_subLR leqNgt addnC.
          apply/negP=> Habs.
          suff: hp_workload_here > R
            by rewrite ltnNge Hworkload.
          have Hbusy t : t1 < t <= R + t1 -> ~ quiet_time tsk t.
          { move=> /andP [Lo Hi].
            apply: Hbw.2.2.
              by rewrite Lo (leq_ltn_trans Hi).
          }
          have ineq: hp_service_here < hp_workload_here.
          { have Ht1Rt1 : t1 <= R + t1 by exact: leq_addl.
            have HbusyR : ~ quiet_time tsk (R + t1) by
                (apply: Hbusy; rewrite leqnn -add1n leq_add).
            have:= busy_pending t1 (R + t1) Ht1Rt1 Hbw.2.1 HbusyR.
            move=> [j [Hinarr [/andP [Lo Hi] [Hprio Hnotcomp]]]].
            have Hj: j \in arrivals_between arrs t1 (R + t1) by
                rewrite (arrivalsP arrs_valid)// Lo Hi arrival_consistent.
            have Huniq: uniq (arrivals_between arrs t1 (R + t1)).
            { rewrite bigcat_nat_uniq //= => [|j' t t' /arrs_valid.2 <- Ht'].
              - exact: arrs_valid.1.
              - exact: (arrs_valid.2 j'); rewrite /arrives_at -?Ht ?Ht'. }
            rewrite /hp_service_here/hp_workload_here/hp_service /hp_workload.
            rewrite big_mkcond [X in _ < X]big_mkcond /=.
            rewrite !(bigD1_seq j) //= Hprio.
            rewrite -add1n addnA [1 + _]addnC addn1 leq_add //=.
            - rewrite ltnNge.
              suff->: service_between sched j t1 (R + t1) = service j (R + t1) by
                  apply/negP => /Hnotcomp.
              rewrite /service /service_between.
              rewrite [in RHS](big_cat_nat _ _ (n:= t1)) //=.
              suff->: \sum_(0 <= t < t1) scheduled_at j t = 0 by [].
              rewrite -{3}(muln0 (t1 - 0)) -sum_nat_const_nat.
              apply: eq_big_nat => t /andP [_ /leq_trans /(_ Lo) H].
              case/boolP: (scheduled_at j t) => [/sched_arrived|//].
                by rewrite leqNgt H.
            - rewrite big_seq_cond [X in _ <= X]big_seq_cond /=.
              apply: leq_sum => j'.
              rewrite (arrivalsP arrs_valid)// => /andP [/andP [/andP [Lo' Hi'] Harr'] Hjj'].
              case: ifP => Hprio' /=; last by [].
              exact: service_between_bound. }
          suff->: R = hp_service_here by [].
          rewrite -[R](addnK t1) -[R + t1 - t1]muln1 -sum_nat_const_nat.
          rewrite /hp_service_here /hp_service /service_between.
          rewrite exchange_big /=.
          apply: eq_big_nat => //= t /andP [Lo Hi].
          have Hquiet: quiet_time tsk t1 by exact: Hbw.2.1.
          have:= busyP t1 (R + t1) (leq_ltn_trans Lo Hi) Hquiet Hbusy t.
          rewrite Lo Hi => /(_ is_true_true) [j [/andP [Lo' Hi'] [Hprio Hsched]]].
          have Harr: in_arr_seq arrs j by exact: (sched_in_arrs j t).
          have Hj: j \in arrivals_between arrs t1 (R + t1) by
              rewrite (arrivalsP arrs_valid)// Lo' Hi' arrival_consistent.
          have Huniq: uniq (arrivals_between arrs t1 (R + t1)).
          { rewrite bigcat_nat_uniq //= => [|j' t' t'' /arrs_valid.2 <- Ht'].
            - exact: arrs_valid.1.
            - exact: (arrs_valid.2 j'). }
          rewrite big_mkcond (bigD1_seq j) //=.
          rewrite Hprio Hsched /= big1 //= => j' /negbTE Hjj'.
          case: hp; last by [].
          rewrite /scheduled_at.
          move: Hsched => /eqP ->.
            by rewrite eqE /= eq_sym Hjj'.
        Qed.
      End BusyBound.

      Lemma busy_interval_length_bound_correct R :
        hp_workload_bound tsk R <= R ->
        R > 0 ->
        forall t1 t2,
          busy_interval_prefix tsk t1 t2 ->
          t2 - t1 <= R.
      Proof.
        move=> Hfix HR t1 t2.
        apply: busy_bound => //=.
        apply: fp_workload_bound; first exact: Hfix.
      Qed.

      Lemma busy_interval_size_bound_correct  R :
        tsk \in ts ->
        hp_workload_bound tsk R <= R ->
        R > 0 ->
        forall t1 t2,
          busy_interval_prefix tsk t1 t2 ->
          size (task_arrivals_between job_task arrs tsk t1 t2) <= eta_plus tsk R.
      Proof.
        move=> Htsk Hfix HR t1 t2 Hbusy.
        have Ht12 : t1 <= t2 by exact: (ltnW Hbusy.1).
        rewrite -(subnK Ht12).
        set arrs' := task_arrivals_between _ _ _.
        have H1 : arrs' t1 (R + t1) = cat (arrs' t1 (t2 - t1 + t1)) (arrs' (t2 - t1 + t1) (R + t1))
          by rewrite -big_cat_nat//= subnK//= addnC -leq_subLR
                     busy_interval_length_bound_correct.
        have H2 : size (arrs' t1 (t2 - t1 + t1)) <= size (arrs' t1 (R + t1))
          by rewrite H1 size_cat leq_addr.
        apply: (leq_trans H2).
        exact: arrival_model.
      Qed.

      Lemma busy_response_time_bound t1 t2 j:
        tsk \in ts ->
        busy_interval tsk t1 t2 ->
        j \in task_arrivals_between job_task arrs tsk t1 t2 ->
        response_time_bound job_arrival job_cost sched (t2 - t1) j.
      Proof.
        move=> Htsk Hbw.
        rewrite (task_arrivalsP arrs_valid)// => /andP[/andP [/eqP Hjtsk /andP[Lo Hi]] Harrj].
        rewrite /response_time_bound.
        set t := job_arrival j.
        have Harrj2 : in_arr_seq arrs j by exists (job_arrival j).
        have Hpend: pending j t.
        { rewrite /pending leqnn /= /completed.
          rewrite -ltnNge /service/service_between.
          rewrite big_nat big1 ?cost_pos//= => t' Ht'.
          case eq: scheduled_at=>//=.
            by rewrite ltnNge sched_arrived in Ht'.
        }
        have Hcomp: completed j t2 by
            rewrite (completes_in_interval t1 t2 j t)//= ?Lo ?Hi ?Hjtsk.
        suff/completion_monotonic ineq: t2 <= t + (t2 - t1) by exact: ineq.
        by rewrite -{1}(subnK (ltnW Hbw.1.1)) addnC leq_add.
      Qed.
    End RTA.
  End BusyWindows.
End Analysis.
